# NAME

showstatus - Just a simple program for generating output to display some information about your system. Intended for use with swaybar.

## DEPENDENCIES

No special dependencies, regular POSIX libraries for Linux

## DESCRIPTION

Every .25 second a line is printed to stdout that will display some system information. This is inteded to be used with applications like swaybar where you call showstatus as the status\_command in your sway config.

This should also work well with similar programs that take input in the same way.

Note that you will need to modify showstatus.h before compiling. The default commands are not guaranteed to work for your system, but they can be a good starting point to help you out.

## BUILDING

Ensure you have the required build tools for your linux distribution, then simply call 'make' in the 'src' directory. This will create the 'showstatus' executable.

## AUTHOR

Daniel Jenssen <daerandin@gmail.com>

