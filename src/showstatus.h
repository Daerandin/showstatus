#ifndef SHOWSTATUS_H
#define SHOWSTATUS_H

/* The following can be modifed to fit your system before compilation. */

/* NOTE! The command outputs can't be larger than 9 characters.
 * The ending newline is not counted or included. */

/* Shell command that will give the GPU temperature in C. */
#define GPU_TEMP "echo scale = 1 \"\n\" $(cat /sys/class/drm/card1/device/hwmon/hwmon0/temp1_input) / 1000 | bc -l"
#define GPU_TEMP_U 1 /* How often it updates (seconds). */

/* The rest follows the same definition pattern. */

/* This waas the old method for getting cpu percentage, but has been replaced by calculating directly from /proc/stat */
/*
#define CPU_PER "LC_ALL=C top -bn2 | awk ' $1 == \"%Cpu(s):\" { val = $8 } ; END { printf(\"%.1f\\n\", 100-val) }'"
#define CPU_PER_U 1
*/

#define CPU_TEMP "echo scale = 1 \"\n\" $(cat /sys/class/hwmon/hwmon3/temp2_input) / 1000 | bc -l"
#define CPU_TEMP_U 1

#define SWP "free | awk ' $1 == \"Swap:\" { printf(\"%.1f\\n\", ($3/$2)*100) }'"
#define SWP_U 1

#define MEM "free | awk ' $1 == \"Mem:\" { printf(\"%.1f\\n\", (($2-$7)/$2)*100) }'"
#define MEM_U 1

#define SSD "btrfs filesystem usage / -b 2>/dev/null | awk ' NR == 2 { total = $3 }; NR == 8 { free = $3 }; END { printf(\"%.1f\\n\", ((total - free)/total) * 100) }'"
#define SSD_U 1

#define SSD2 "btrfs filesystem usage /home/sheep -b 2>/dev/null | awk ' NR == 2 { total = $3 }; NR == 8 { free = $3 }; END { printf(\"%.1f\\n\", ((total - free)/total) * 100) }'"
#define SSD2_U 1

#define VPN "pgrep --exact openvpn | awk 'END { print $0 ? \"UP\" : \"DOWN\" }'"
#define VPN_U 1

#define WIFI "iwctl station list 2>/dev/null | awk ' $2 == \"wlan0\" { state = $3 }; END { print state == \"connected\" ? \"UP\" : \"DOWN\" }'"
#define WIFI_U 1

#define AUR "auracle outdated | wc -l"
#define AUR_U 60*30 /* Do not check for updates too frequently, this causes undue strain on mirrors and is generally considered a dick move. */

#define UPD "checkupdates | wc -l"
#define UPD_U 60*30 /* Do not check for updates too frequently, this causes undue strain on mirrors and is generally considered a dick move. */

void mainloop(void);

#endif /* SHOWSTATUS_H */
