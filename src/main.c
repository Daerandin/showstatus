/****************************************************************************
*                                                                           *
*  simplestatus - Display system information, created for waybar            *
*  Copyright (C) 2023  Daniel Jenssen <daerandin@gmail.com>                 *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

#include <stdlib.h>
#include "showstatus.h"

int
main(void)
{
    mainloop();
    /* The mainloop is never inteded to finish, so in essence we should never
     * reach the following return statement. */
    return EXIT_SUCCESS;
}
