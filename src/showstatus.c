/****************************************************************************
*                                                                           *
*  simplestatus - Display system information, created for waybar            *
*  Copyright (C) 2023  Daniel Jenssen <daerandin@gmail.com>                 *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "showstatus.h"


static void *get_ye_time(void *);
static void *get_proc_out(void *);
static void *get_ye_cpu(void *);

struct thread_arg {
    char *var;
    pthread_mutex_t *lock;
    time_t sec;
    char *cstring;
};

/* This function controls the mainloop of the program. It is likely to never
 * return as it is inteded to run forever. */
void
mainloop(void)
{

    pthread_t upd_t;
    pthread_t aur_t;
    pthread_t wif_t;
    pthread_t vpn_t;
    pthread_t ssd_t;
    pthread_t ssd2_t;
    pthread_t mem_t;
    pthread_t swp_t;
    pthread_t cpu_per_t;
    pthread_t cpu_tmp_t;
    pthread_t gpu_t;
    pthread_t tim_t;

    char upd[10] = "0";
    char aur[10] = "0";
    char wif[10] = "DOWN";
    char vpn[10] = "DOWN";
    char ssd[10] = "0";
    char ssd2[10] = "0";
    char mem[10] = "0";
    char swp[10] = "0";
    char cpu_per[10] = "0";
    char cpu_tmp[10] = "0";
    char gpu[10] = "0";
    char tim[20] = "1970-01-01 00:00:00";

    /* Mutex locks for each variable. */
    pthread_mutex_t upd_lock;
    pthread_mutex_t aur_lock;
    pthread_mutex_t wif_lock;
    pthread_mutex_t vpn_lock;
    pthread_mutex_t ssd_lock;
    pthread_mutex_t ssd2_lock;
    pthread_mutex_t mem_lock;
    pthread_mutex_t swp_lock;
    pthread_mutex_t cpu_per_lock;
    pthread_mutex_t cpu_tmp_lock;
    pthread_mutex_t gpu_lock;
    pthread_mutex_t tim_lock;

    struct thread_arg upd_a;
    struct thread_arg aur_a;
    struct thread_arg wif_a;
    struct thread_arg vpn_a;
    struct thread_arg ssd_a;
    struct thread_arg ssd2_a;
    struct thread_arg mem_a;
    struct thread_arg swp_a;
    struct thread_arg cpu_per_a;
    struct thread_arg cpu_tmp_a;
    struct thread_arg gpu_a;
    struct thread_arg tim_a;

    struct timespec slp, rem;

    slp.tv_sec = 0;
    slp.tv_nsec = 250000000;
    rem.tv_sec = 0;
    rem.tv_nsec = 0;

    /* Initialize mutexes, exit on any failure. */
    if (pthread_mutex_init(&upd_lock, NULL) ||
        pthread_mutex_init(&aur_lock, NULL) ||
        pthread_mutex_init(&wif_lock, NULL) ||
        pthread_mutex_init(&vpn_lock, NULL) ||
        pthread_mutex_init(&ssd_lock, NULL) ||
        pthread_mutex_init(&ssd2_lock, NULL) ||
        pthread_mutex_init(&mem_lock, NULL) ||
        pthread_mutex_init(&swp_lock, NULL) ||
        pthread_mutex_init(&cpu_per_lock, NULL) ||
        pthread_mutex_init(&cpu_tmp_lock, NULL) ||
        pthread_mutex_init(&gpu_lock, NULL) ||
        pthread_mutex_init(&tim_lock, NULL)) {
        fprintf(stderr,
                "Error: showstatus.c:mainloop: Could not initialize mutex.\n");
        exit(EXIT_FAILURE);
    }

    tim_a.var = tim;
    tim_a.lock = &tim_lock;
    tim_a.cstring = NULL;
    tim_a.sec = 0;

    gpu_a.var = gpu;
    gpu_a.lock = &gpu_lock;
    gpu_a.sec = GPU_TEMP_U;
    gpu_a.cstring = GPU_TEMP;

    cpu_per_a.var = cpu_per;
    cpu_per_a.lock = &cpu_per_lock;
    cpu_per_a.sec = 0;
    cpu_per_a.cstring = NULL;

    cpu_tmp_a.var = cpu_tmp;
    cpu_tmp_a.lock = &cpu_tmp_lock;
    cpu_tmp_a.sec = CPU_TEMP_U;
    cpu_tmp_a.cstring = CPU_TEMP;

    swp_a.var = swp;
    swp_a.lock = &swp_lock;
    swp_a.sec = SWP_U;
    swp_a.cstring = SWP;

    mem_a.var = mem;
    mem_a.lock = &mem_lock;
    mem_a.sec = MEM_U;
    mem_a.cstring = MEM;

    ssd_a.var = ssd;
    ssd_a.lock = &ssd_lock;
    ssd_a.sec = SSD_U;
    ssd_a.cstring = SSD;

    ssd2_a.var = ssd2;
    ssd2_a.lock = &ssd2_lock;
    ssd2_a.sec = SSD2_U;
    ssd2_a.cstring = SSD2;

    wif_a.var = wif;
    wif_a.lock = &wif_lock;
    wif_a.sec = WIFI_U;
    wif_a.cstring = WIFI;

    vpn_a.var = vpn;
    vpn_a.lock = &vpn_lock;
    vpn_a.sec = VPN_U;
    vpn_a.cstring = VPN;

    upd_a.var = upd;
    upd_a.lock = &upd_lock;
    upd_a.sec = UPD_U;
    upd_a.cstring = UPD;

    aur_a.var = aur;
    aur_a.lock = &aur_lock;
    aur_a.sec = AUR_U;
    aur_a.cstring = AUR;

    if (pthread_create(&tim_t, NULL, &get_ye_time, (void *)&tim_a) ||
        pthread_create(&gpu_t, NULL, &get_proc_out, (void *)&gpu_a) ||
        pthread_create(&cpu_per_t, NULL, &get_ye_cpu, (void *)&cpu_per_a) ||
        pthread_create(&cpu_tmp_t, NULL, &get_proc_out, (void *)&cpu_tmp_a) ||
        pthread_create(&swp_t, NULL, &get_proc_out, (void *)&swp_a) ||
        pthread_create(&mem_t, NULL, &get_proc_out, (void *)&mem_a) ||
        pthread_create(&ssd_t, NULL, &get_proc_out, (void *)&ssd_a) ||
        pthread_create(&ssd2_t, NULL, &get_proc_out, (void *)&ssd2_a) ||
        pthread_create(&vpn_t, NULL, &get_proc_out, (void *)&vpn_a) ||
        pthread_create(&upd_t, NULL, &get_proc_out, (void *)&upd_a) ||
        pthread_create(&aur_t, NULL, &get_proc_out, (void *)&aur_a) ||
        pthread_create(&wif_t, NULL, &get_proc_out, (void *)&wif_a)) {
        fprintf(stderr,
                "Error: showstatus.c: mainloop: Could not create thread.\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        if (pthread_mutex_lock(&upd_lock) ||
            pthread_mutex_lock(&aur_lock) ||
            pthread_mutex_lock(&wif_lock) ||
            pthread_mutex_lock(&vpn_lock) ||
            pthread_mutex_lock(&ssd_lock) ||
            pthread_mutex_lock(&ssd2_lock) ||
            pthread_mutex_lock(&mem_lock) ||
            pthread_mutex_lock(&swp_lock) ||
            pthread_mutex_lock(&cpu_per_lock) ||
            pthread_mutex_lock(&cpu_tmp_lock) ||
            pthread_mutex_lock(&gpu_lock) ||
            pthread_mutex_lock(&tim_lock)) {
            fprintf(stderr, "Error: showstatus.c:mainloop: Error while "
                            "attempting to lock mutex.\n");
            exit(EXIT_FAILURE);
        }
        fflush(stdout);
        printf("UPD: %s AUR: %s | WIFI: %s VPN: %s | SSD1: %s%% SSD2: %s%% | MEM: %s%% "
               "SWAP: %s%% | CPU: %s%% %s°C | GPU: %s°C | %s \n",
               upd, aur, wif, vpn, ssd, ssd2, mem, swp, cpu_per, cpu_tmp, gpu, tim);
        if (pthread_mutex_unlock(&upd_lock) ||
            pthread_mutex_unlock(&aur_lock) ||
            pthread_mutex_unlock(&wif_lock) ||
            pthread_mutex_unlock(&vpn_lock) ||
            pthread_mutex_unlock(&ssd_lock) ||
            pthread_mutex_unlock(&ssd2_lock) ||
            pthread_mutex_unlock(&mem_lock) ||
            pthread_mutex_unlock(&swp_lock) ||
            pthread_mutex_unlock(&cpu_per_lock) ||
            pthread_mutex_unlock(&cpu_tmp_lock) ||
            pthread_mutex_unlock(&gpu_lock) ||
            pthread_mutex_unlock(&tim_lock)) {
            fprintf(stderr, "Error: showstatus.c:mainloop: Error while "
                            "attempting to unlock mutex.\n");
            exit(EXIT_FAILURE);
        }
        nanosleep(&slp, &rem);
    }

}

static void *
get_ye_time(void *args)
{
    struct thread_arg *ta = (struct thread_arg *)args;
    time_t ye_time = -1;
    struct tm split_time;
    struct timespec slp;

    slp.tv_sec = 0;
    slp.tv_nsec = 250000000;

    while (1) {
        if ((ye_time = time(NULL)) == -1) {
            fprintf(stderr, "Error: showstatus.c:get_ye_time: Error while "
                            "attempting to get time.\n");
            exit(EXIT_FAILURE);
        }
        if (!(localtime_r(&ye_time, &split_time))) {
            fprintf(stderr, "Error: showstatus.c:get_ye_time: Error while "
                            "attempting to split time.\n");
            exit(EXIT_FAILURE);
        }
        if (pthread_mutex_lock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_ye_time: Error while "
                            "attempting to lock mutex.\n");
            exit(EXIT_FAILURE);
        }
        snprintf(ta->var, 20, "%04u-%02u-%02u %02u:%02u:%02u",
                 (unsigned int)split_time.tm_year + 1900,
                 (unsigned int)split_time.tm_mon + 1,
                 (unsigned int)split_time.tm_mday,
                 (unsigned int)split_time.tm_hour,
                 (unsigned int)split_time.tm_min,
                 (unsigned int)split_time.tm_sec);
        if (pthread_mutex_unlock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_ye_time: Error while "
                            "attempting to unlock mutex.\n");
            exit(EXIT_FAILURE);
        }
        nanosleep(&slp, NULL);
    }
    return NULL;
}

static void *
get_proc_out(void *args)
{
    struct thread_arg *ta = (struct thread_arg *)args;
    char inp[10];
    int i, c;
    FILE *sp;
    struct timespec slp;

    slp.tv_sec = ta->sec;
    slp.tv_nsec = 0;

    while (1) {
        if (!(sp = popen(ta->cstring, "r"))) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Error "
                    "while attempting to run command:\n%s\n", ta->cstring);
            exit(EXIT_FAILURE);
        }
        i = 0;
        while ((c = fgetc(sp)) != EOF && i < 9 ) {
            if (c != '\n')
                inp[i++] = (char)c;
        }
        pclose(sp);
        if (c != EOF) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Too big"
                            " output for buffer from command: %s\n",
                            ta->cstring);
            exit(EXIT_FAILURE);
        }
        inp[i] = '\0';

        if (pthread_mutex_lock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Error "
                            "while attempting to lock mutex.\n");
            exit(EXIT_FAILURE);
        }
        snprintf(ta->var, 10, "%s", inp);
        if (pthread_mutex_unlock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Error "
                            "while attempting to unlock mutex.\n");
            exit(EXIT_FAILURE);
        }
        nanosleep(&slp, NULL);
    }
    return NULL;
}

static void *
get_ye_cpu(void *args)
{
    struct thread_arg *ta = (struct thread_arg *)args;
    const char fpath[] = "/proc/stat";
    FILE *fp;
    int r;
    unsigned long long prevfullidle, fullidle, prevtotal, total, previdle,
                       previowait, idle, iowait, prevnonidle, prevuser,
                       prevnice, prevsystem, previrq, prevsoftirq, prevsteal,
                       nonidle, user, nice, system, irq, softirq, steal,
                       totaldiff, idlediff;
    struct timespec slp;

    slp.tv_sec = 1;
    slp.tv_nsec = 0;

    while (1) {
        if (!(fp = fopen(fpath, "r"))) {
            fprintf(stderr, "Error: showstatus.c:get_ye_cpu: Can't open %s "
                            "for reading.\n", fpath);
            exit(EXIT_FAILURE);
        }
        r = fscanf(fp, "cpu  %llu %llu %llu %llu %llu %llu %llu %llu ",
               &prevuser, &prevnice, &prevsystem, &previdle, &previowait,
               &previrq, &prevsoftirq, &prevsteal);
        if (r == EOF) {
            fprintf(stderr, "Error: showstatus.c:get_ye_cpu: Can't read cpu "
                            "values from file: %s", fpath);
            exit(EXIT_FAILURE);
        }
        fclose(fp);
        nanosleep(&slp, NULL);
        if (!(fp = fopen(fpath, "r"))) {
            fprintf(stderr, "Error: showstatus.c:get_ye_cpu: Can't open %s "
                            "for reading.\n", fpath);
            exit(EXIT_FAILURE);
        }
        r = fscanf(fp, "cpu  %llu %llu %llu %llu %llu %llu %llu %llu ",
               &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal);
        if (r == EOF) {
            fprintf(stderr, "Error: showstatus.c:get_ye_cpu: Can't read cpu "
                            "values from file: %s", fpath);
            exit(EXIT_FAILURE);
        }
        fclose(fp);

        prevfullidle = previdle + previowait;
        fullidle = idle + iowait;
        prevnonidle = prevuser + prevnice + prevsystem +
                      previrq + prevsoftirq + prevsteal;
        nonidle = user + nice + system + irq + softirq + steal;
        prevtotal = prevfullidle + prevnonidle;
        total = fullidle + nonidle;
        totaldiff = total - prevtotal;
        idlediff = fullidle - prevfullidle;

        if (pthread_mutex_lock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Error "
                            "while attempting to lock mutex.\n");
            exit(EXIT_FAILURE);
        }
        snprintf(ta->var, 10, "%.1Lf",
                 (long double)(totaldiff - idlediff)/totaldiff * 100);
        if (pthread_mutex_unlock(ta->lock)) {
            fprintf(stderr, "Error: showstatus.c:get_proc_out: Error "
                            "while attempting to unlock mutex.\n");
            exit(EXIT_FAILURE);
        }
    }
    return NULL;
}
